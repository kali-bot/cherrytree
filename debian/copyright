Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: cherrytree
Source: https://github.com/giuspen/cherrytree

Files: *
Copyright: 2009-2019 Giuseppe Penone <giuspen@gmail.com>
License: GPL-3+

Files: modules/pgsc_locales.py
       modules/pgsc_spellcheck.py
Copyright: 2012, Maximilian Köhl <linuxmaxi@googlemail.com>
           2012, Carlos Jenkins <carlos@jenkins.co.cr>
License: GPL-3+

Files: future/src/7za/C/*
Copyright: Igor Pavlov
License: Public-Domain
 Igor Pavlov : Public domain

Files: future/src/fmt/*
Copyright: 2012-2019 Victor Zverovich
License: BSD-2-clause

Files: future/src/fmt/fmt.h
Copyright: 2017-2019 Giuseppe Penone <giuspen@gmail.com>
License: GPL-3+

Files: future/src/7za/CPP/myWindows/wine_date_and_time.cpp
Copyright: 1999-2015, Igor Pavlov
License: LGPL-2.1+

Files: language-specs/go.lang
Copyright: 2009-2010 Jim Teeuwen <jimteeuwen@gmail.com>
           2010 Kenny Meyer <knny.myer@gmail.com>
License: LGPL-2.1+

Files: debian/*
Copyright: 2011-2014 Vincent Cheng <vcheng@debian.org>
           2017-2019 Sophie Brun <sophie@offensive-security.com>
License: GPL-3+

License: GPL-3+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License version 2.1 can be found in
 "/usr/share/common-licenses/LGPL-2.1".

License: BSD-2-clause
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
